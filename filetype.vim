augroup filetypedetect
au BufNewFile,BufRead *.tjp,*.tji  setf tjp
au BufNewFile,BufRead *.vhd,*.vhdl setf vhdl
augroup END
